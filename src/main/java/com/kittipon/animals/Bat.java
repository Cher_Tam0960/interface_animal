/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.animals;

/**
 *
 * @author kitti
 */
public class Bat extends PoultryAnimal {

    private String nickname;

    public Bat(String nickname) {
        super();
        this.nickname = nickname;
    }

    @Override
    public void fly() {
        System.out.println("Bat : " + nickname + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bat : " + nickname + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Bat : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat : " + nickname + " sleep");
    }

}
