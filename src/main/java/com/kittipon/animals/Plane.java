/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.animals;

/**
 *
 * @author kitti
 */
public class Plane extends Vahicle implements Flyable,Runable {

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("plane : start engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("plane : stop engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("plane : speed up!!!");
    }

    @Override
    public void applyBreak() {
        System.out.println("plane : speed down!!!");
    }

    @Override
    public void fly() {
        System.out.println("plane : fly");
    }

    @Override
    public void run() {
        System.out.println("plane : run");
    }

}
