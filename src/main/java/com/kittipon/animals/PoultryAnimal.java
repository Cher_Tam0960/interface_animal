/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.animals;

/**
 *
 * @author kitti
 */
public abstract class PoultryAnimal extends Animal implements Flyable{

    public PoultryAnimal() {
        super("Poultry", 2);
    }
    public abstract void fly();
}
